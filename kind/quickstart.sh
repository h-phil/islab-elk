#!/bin/bash
user=$(whoami)
set -eou pipefail
echo "kind should only be used for testing!"
sudo kind delete cluster --name islab
sudo podman network rm kind || true
sudo kind create cluster --name islab --config cluster.yml
sudo kind get kubeconfig --name islab > config

sudo chown $user:$user config
export KUBECONFIG=./config

echo "apply ingress nginx"
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml

echo "waiting for ingress to fully start. May take a while ... (timeout 180s)"
sleep 10 # we need to sleep, if the selector does not find a pod it will just continue
kubectl wait --namespace ingress-nginx \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=180s

echo "apply argocd"
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

echo "waiting for argocd to fully start. May take a while ... (timeout 180s)"
sleep 10 # we need to sleep, if the selector does not find a pod it will just continue
kubectl wait --namespace argocd \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/name=argocd-server \
  --timeout=180s

echo "apply ingress for argo"
kubectl -n argocd apply -f argocd-ingress.yml

echo "apply metallb config"
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.12.1/manifests/namespace.yaml
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.12.1/manifests/metallb.yaml
sleep 10
kubectl wait --namespace metallb-system \
  --for=condition=ready pod \
  --selector=app=metallb \
  --timeout=180s

kubectl apply -f metallb-configmap.yml

echo "initial argo password: "
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
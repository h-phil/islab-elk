## Cluster (k8s with kind) + Prerequisites

## Tools/Software
- Kind (k8s): https://kind.sigs.k8s.io
- Helm: https://helm.sh/
- ArgoCD: https://argoproj.github.io/argo-cd/
- Kubectl: https://kubernetes.io/de/docs/tasks/tools/install-kubectl/
- Podman: https://podman.io/
- Nginx Ingress: https://github.com/kubernetes/ingress-nginx

## Installation Setup
### Podman 
```bash
# docker can also be used
dnf install podman
```
### Kind

```bash
# make sure to use the latest version
# see: https://kind.sigs.k8s.io/docs/user/quick-start#installation

curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.12.0/kind-linux-amd64
chmod +x ./kind
mv ./kind /usr/local/bin/kind
```

### Kubectl
```bash
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
echo "$(<kubectl.sha256) kubectl" | sha256sum --check
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```

## Cluster Setup
```
./quickstart.sh
```